# Branching strategy
#### Base branch : master

# How to Create branch?

### Branch should follow any of the below prefix.
* feat: (new feature for the user, not a new feature for build script)
* fix: (bug fix for the user, not a fix to a build script)
* test: (adding missing tests, refactoring tests; no production code change)

examples: 

``` 
feat/heading-component 
```
``` 
fix/homepage-header-issue 
```

# The commit message should be in the below format

### Example 1:
```
feat(header): header component
1) develop header component
2) responsive support added
3) content api integrated
4) autocomplete search implemented 
```
### Example 2: 
```
fix(carousel): issues
1) fix next and previous button functionality
2) fix background color as per the design
```

Target branch for merge requests will be the base (master) branch.
