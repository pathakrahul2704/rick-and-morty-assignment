import React from 'react'
import useFetch from 'react-fetch-hook'
import Homepage from './components/templates/Homepage/homepage'
import './styles/main.scss'
import { Provider } from './store/store'

const App = () => {
  const { isLoading, data: contentData } = useFetch(
    'https://rickandmortyapi.com/api/character/ '
  )
  const { data: staticData } = useFetch('/public/content/en-us.json')

  if (isLoading) {
    return 'loading...'
  }
  return (
    <Provider value={{ staticData, contentData, isLoading }}>
      <Homepage />
    </Provider>
  )
}

export default App
