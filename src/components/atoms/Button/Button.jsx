import PropTypes from 'prop-types'

/**
 * @description its a Button tag
 * @param {node} children dom inside the button component
 * @param  {func} onClick  performs click on a button
 * @param   {string} buttonStyle  custom style for a button
 * @param   {string} testId usde for testing the component
 * @return    button tag is returned
 */

const Button = ({ children, onClick, buttonStyle, testId }) => {
  return (
    <button className={buttonStyle} data-testid={testId} onClick={onClick}>
      {children}
    </button>
  )
}
Button.propTypes = {
  onClick: PropTypes.func,
  buttonStyle: PropTypes.string,
  testId: PropTypes.string,
  children: PropTypes.node
}

Button.defaultProps = {
  testId: 'button'
}

export default Button
