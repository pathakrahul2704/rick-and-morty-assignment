import PropTypes from 'prop-types'
import './styles.scss'
/**
 * @description its a Checbox component
 * @param {string} name label of a check box
 * @param  {func} toggle  performs onchange on a checkbox
 * @param   {bool} checked  used to set the check box
 * @param   {string} testId usde for testing the component
 * @return    Checkbox tag is returned
 */
const Checkbox = ({ name, toggle, value, checked, testId }) => {
  return (
    <div className='checkbox-box'>
      <input
        data-testid={testId}
        type='checkbox'
        id={name}
        onChange={toggle}
        name={name}
        value={value}
        checked={checked}
      />
      <label htmlFor={name}>{name}</label>
    </div>
  )
}

Checkbox.propTypes = {
  toggle: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.bool,
  checked: PropTypes.bool,
  testId: PropTypes.string
}
export default Checkbox
