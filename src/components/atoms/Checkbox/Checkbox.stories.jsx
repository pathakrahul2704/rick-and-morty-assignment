import { Checkbox } from './Checkbox'

export default {
  title: 'Atoms/Checkbox',
  component: Checkbox,
  argTypes: {
    name: 'Human'
  }
}

const Template = (args) => <Checkbox {...args} />

export const Primary = Template.bind({})
Primary.args = {
  name: 'Human'
}
