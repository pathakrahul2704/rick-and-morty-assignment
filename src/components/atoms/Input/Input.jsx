import PropTypes from 'prop-types'

/**
 * @description its a Input component
 * @param {string} labelName label of a input tag
 * @param {string} name name of a input tag
 * @param {string} type type of a input tag
 * @param  {func} handleChange  performs onchange on a input
 * @param   {string} testId usde for testing the component
 * @return    Input tag is returned
 */
const Input = ({ labelName, name, inputStyle, type, handleChange }) => {
  return (
    <label>
      {labelName}
      <input
        data-testid='search-input'
        aria-label={labelName}
        aria-required='true'
        onChange={handleChange}
        name={name}
        className={inputStyle}
        type={type}
      />
    </label>
  )
}
Input.propTypes = {
  labelName: PropTypes.string,
  name: PropTypes.string,
  inputStyle: PropTypes.string,
  type: PropTypes.string,
  handleChange: PropTypes.func
}

Input.defaultProps = {
  labelName: 'name'
}

export default Input
