import { Input } from './Input'

export default {
  title: 'Atoms/Input',
  component: Input,
  argTypes: {
    labelName: 'name'
  }
}

const Template = (args) => <Input {...args} />

export const Primary = Template.bind({})
Primary.args = {
  labelName: 'name'
}
