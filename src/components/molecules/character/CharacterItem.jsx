import PropTypes from 'prop-types'
import './style.scss'

/**
 * @description its a Character card component
 * @param {object} character contains individual character data
 * @return    displays data of an individual character
 */
const CharacterItem = ({ character }) => {
  const characterCreatedTime =
    new Date().getFullYear() - new Date(character.created).getFullYear()
  return (
    <div className='character'>
      <div className='pos-relative'>
        <img
          className='character-image'
          src={character.image}
          alt={character.name}
        />
        <div className='character-name'>
          <p>{character.name}</p>
          <p>{`id: ${character.id} - created ${characterCreatedTime} years ago`}</p>
        </div>
      </div>
      <div className='character-details'>
        <div className='character-details-individual'>
          <p className='font-white'>STATUS</p>
          <p className='character-data'>{character.status}</p>
        </div>
        <div className='character-details-individual'>
          <p className='font-white'>SPECIES</p>
          <p className='character-data'>{character.species}</p>
        </div>
        <div className='character-details-individual'>
          <p className='font-white'>GENDER</p>
          <p className='character-data'>{character.gender}</p>
        </div>
        <div className='character-details-individual'>
          <p className='font-white'>ORIGIN</p>
          <p className='character-data'>{character.origin.name}</p>
        </div>
        <div className='character-details-individual'>
          <p className='font-white'>LOCATION</p>
          <p className='character-data'>{character.location.name}</p>
        </div>
      </div>
    </div>
  )
}
CharacterItem.propTypes = {
  character: PropTypes.object
}
export default CharacterItem
