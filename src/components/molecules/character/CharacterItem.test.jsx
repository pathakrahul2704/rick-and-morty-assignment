import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import CharacterItem from './CharacterItem'
import { mockResponse } from '../../../../__mocks__/mock'

describe('CharacterItem.jsx', () => {
  test('should render Character Item component', () => {
    render(<CharacterItem character={mockResponse.results[0]} />)
    expect(screen.getByText(mockResponse.results[0].name)).toBeInTheDocument()
  })
})
