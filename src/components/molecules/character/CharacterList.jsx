import PropTypes from 'prop-types'
import CharacterItem from './CharacterItem'

/**
 * @description its a Character list component
 * @param {object} character contains all the characters data
 * @return    displays data of all characters
 */
const CharacterList = ({ characterData }) => {
  return (
    <>
      {characterData &&
        characterData.length &&
        characterData.map((character) => (
          <CharacterItem character={character} key={character.id} />
        ))}
    </>
  )
}

CharacterList.propTypes = {
  characterData: PropTypes.array
}
export default CharacterList
