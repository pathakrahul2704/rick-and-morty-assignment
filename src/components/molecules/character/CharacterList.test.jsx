import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import CharacterList from './CharacterList'
import { mockResponse } from '../../../../__mocks__/mock'

describe('CharacterList.jsx', () => {
  test('should render list ', () => {
    render(<CharacterList characterData={mockResponse?.results} />)
  })
})
