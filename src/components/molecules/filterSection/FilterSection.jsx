import Checkbox from '../../atoms/Checkbox/Checkbox'
import PropTypes from 'prop-types'
import './style.scss'
import { Fragment, useState } from 'react'
import Button from '../../atoms/Button/Button'
import plus from '../../../../public/requirement/plus.png'

/**
 * @description its a side filter component
 * @param {object} staticData contains all the static data
 * @param {array} species contains all the filters of species
 * @param {array} gender contains all the filters of gnedr
 * @param {array} origin contains all the filters of origin
 * @param {func} handleFilters sends all the selected filters array to parent component
 * @return  displays filters applied to all characters
 */

const FilterSection = ({
  handleFilters,
  species,
  gender,
  origin,
  selectedFilters,
  staticData
}) => {
  const [showFilters, toggleFilters] = useState(true)

  function handleSelectedFilters(value) {
    const currentIndex = selectedFilters.indexOf(value)
    const newChecked = [...selectedFilters]

    if (currentIndex == -1) {
      newChecked.push(value)
    } else {
      newChecked.splice(currentIndex, 1)
    }
    // sending the filters to parent component
    handleFilters && handleFilters(newChecked)
  }
  return (
    <>
      <div className='filter'>
        <div className='filter-button-box'>
          <p>Filters</p>
          <div>
            <Button
              testId='filter-toggle'
              buttonStyle='filter-button'
              text='+'
              onClick={() => toggleFilters(!showFilters)}
            >
              <img width='20' height='20' src={plus} alt='plus' />
            </Button>
          </div>
        </div>
      </div>
      {showFilters ? (
        <Fragment>
          <div className='filter-box' data-testid='filter-box'>
            <p>{staticData?.filter?.[0]}</p>
            {species.map((filter, index) => {
              return (
                <Fragment key={index}>
                  <Checkbox
                    testId='species-filter'
                    toggle={() => handleSelectedFilters(filter.name)}
                    checked={
                      selectedFilters &&
                      selectedFilters.indexOf(filter.name) == -1
                        ? false
                        : true
                    }
                    name={filter.name}
                  />
                </Fragment>
              )
            })}
          </div>
          <div className='filter-box'>
            <p>{staticData?.filter?.[1]}</p>
            {gender.map((filter, index) => {
              return (
                <Fragment key={index}>
                  <Checkbox
                    testId='gender-filter'
                    toggle={() => handleSelectedFilters(filter.name)}
                    checked={
                      selectedFilters &&
                      selectedFilters.indexOf(filter.name) == -1
                        ? false
                        : true
                    }
                    name={filter.name}
                  />
                </Fragment>
              )
            })}
          </div>
          <div className='filter-box'>
            <p>{staticData?.filter?.[2]}</p>
            {origin.map((filter, index) => {
              return (
                <Fragment key={index}>
                  <Checkbox
                    testId='origin-filter'
                    toggle={() => handleSelectedFilters(filter.name)}
                    checked={
                      selectedFilters &&
                      selectedFilters.indexOf(filter.name) == -1
                        ? false
                        : true
                    }
                    name={filter.name}
                  />
                </Fragment>
              )
            })}
          </div>
        </Fragment>
      ) : (
        <Fragment />
      )}
    </>
  )
}
FilterSection.propTypes = {
  handleFilters: PropTypes.func,
  species: PropTypes.array,
  origin: PropTypes.array,
  gender: PropTypes.array,
  selectedFilters: PropTypes.array,
  staticData: PropTypes.object
}
export default FilterSection
