import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import FilterSection from './FilterSection'
export const species = [
  {
    id: 1,
    name: 'Human'
  },
  {
    id: 2,
    name: 'Alien'
  }
]
export const gender = [
  {
    id: 1,
    name: 'Male'
  },
  {
    id: 2,
    name: 'Female'
  }
]
export const origin = [
  {
    id: 1,
    name: 'Earth (C-137)'
  },
  {
    id: 2,
    name: 'Earth (Replacement Dimension)'
  },
  { id: 3, name: 'Abadango' },
  { id: 4, name: 'unknown' }
]

describe('FilterSection.jsx', () => {
  test('should render filter section ', () => {
    render(<FilterSection species={species} origin={origin} gender={gender} />)
    expect(screen.getByText(/Filters/i)).toBeInTheDocument()
  })
  test('toggle filter section ', () => {
    render(<FilterSection species={species} origin={origin} gender={gender} />)
    const filterDiv = screen.getByTestId('filter-box')
    const plusButton = screen.getByTestId('filter-toggle')
    fireEvent.click(plusButton)
    expect(filterDiv).not.toBeInTheDocument()
  })
})
