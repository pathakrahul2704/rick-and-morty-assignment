import Input from '../../atoms/Input/Input'
import Button from '../../atoms/Button/Button'
import PropTypes from 'prop-types'
import './style.scss'

/**
 * @description its a search component
 * @param {func} search searches the character based on typed text
 * @param {func} setSearchedText sends the typed text to parent component
 * @return  displays search input and button
 */

const Search = ({ setSearchedText, search }) => {
  return (
    <div className='search-box'>
      <Input
        labelName='Search by Name'
        name='text'
        type='name'
        handleChange={setSearchedText}
        inputStyle='inputbox'
      />
      <Button testId='search-button' buttonStyle='search-but' onClick={search}>
        Search
      </Button>
    </div>
  )
}
Search.propTypes = {
  search: PropTypes.func,
  setSearchedText: PropTypes.func
}
export default Search
