import PropTypes from 'prop-types'
import './style.scss'

/**
 * @description its a sort component
 * @param {func} onChange handles wheather to sort characters in asc or desc order
 * @return  displays select tag
 */
const Sort = ({ onChange }) => {
  return (
    <select
      data-testid='character-sort'
      className='select-box'
      onChange={onChange}
    >
      <option
        data-testid='select-option'
        value='sort by id'
        disabled
        defaultValue
      >
        sort by id
      </option>
      <option data-testid='character-asc' value='asc'>
        Ascending
      </option>
      <option data-testid='character-dsc' value='dsc'>
        Descending
      </option>
    </select>
  )
}

Sort.propTypes = {
  onChange: PropTypes.func
}
export default Sort
