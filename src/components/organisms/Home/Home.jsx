import { useEffect, useState, useContext, Fragment } from 'react'
import './style.scss'
import FilterSection from '../../molecules/filterSection/FilterSection'
import LocalStateContext from '../../../store/store'
import Sort from '../../molecules/sort/Sort'
import Search from '../../molecules/search/Search'
import CharacterList from '../../molecules/character/CharacterList'
import Button from '../../atoms/Button/Button'
import { species, origin, gender } from '../../../utils/staticData'

const Homepage = () => {
  // getting data from context api
  const { contentData, staticData } = useContext(LocalStateContext)
  const [characterData, setCharacterData] = useState([])
  const [selectedFilters, setSelectedFilters] = useState([])
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    // setting data from context api to state
    setCharacterData(contentData?.results)
  }, [contentData])

  // search character by name
  function search() {
    let lowerCasedValue = searchText.toLowerCase()
    let result = []
    const originalData = contentData?.results
    // filtering the state data according to the searched text
    result =
      originalData &&
      originalData.length &&
      originalData.filter((data) => {
        return data.name.toLowerCase().includes(lowerCasedValue)
      })
    setCharacterData(result)
  }

  // sort character by id
  function sort(e) {
    let result = []
    const oldData = characterData
    // sort by ascending order
    if (e.target.value == 'asc') {
      result = oldData.slice().sort((a, b) => {
        return a.id - b.id
      })
    }
    // sort by descending order
    else {
      result = oldData.slice().sort((a, b) => {
        return b.id - a.id
      })
    }
    setCharacterData(result)
  }
  function removeFilter(value) {
    const currentIndex = selectedFilters.indexOf(value)
    const filterCopy = [...selectedFilters]
    filterCopy.splice(currentIndex, 1)
    setSelectedFilters(filterCopy)
    handleFilters(filterCopy)
  }

  // setting the text that is to be searched
  function setSearchedText(e) {
    if (e.target.value === '') {
      /*  if seachedtext is null and filters present
      apply filter on state data */
      console.log(selectedFilters, 'selectedFilters')
      if (selectedFilters && selectedFilters.length > 0) {
        return handleFilters(selectedFilters)
      }
      // else no filters present set state data to data from context api
      return setCharacterData(contentData?.results)
    }
    // else set the searched text to state
    return setSearchText(e.target.value)
  }

  // handling species filter
  function handleFilters(checkedFilters) {
    // set all the fileters present to state
    setSelectedFilters(checkedFilters)
    let result = []
    const speciesFilterPresent =
      checkedFilters.includes('Human') || checkedFilters.includes('Alien')

    const genderFilterPresent =
      checkedFilters.includes('Male') || checkedFilters.includes('Female')

    const originFilterPresent =
      checkedFilters.includes('Earth (C-137)') ||
      checkedFilters.includes('Earth (Replacement Dimension)') ||
      checkedFilters.includes('Abadango') ||
      checkedFilters.includes('unknown')
    // checking if any filter present

    if (checkedFilters.length) {
      if (speciesFilterPresent) {
        result = contentData?.results.filter((data) => {
          return checkedFilters.indexOf(data.species) > -1
        })
      }
      if (genderFilterPresent) {
        if (result && result.length) {
          result = result.filter((data) => {
            return checkedFilters.indexOf(data.gender) > -1
          })
        } else {
          result = contentData?.results.filter((data) => {
            return checkedFilters.indexOf(data.gender) > -1
          })
        }
      }
      if (originFilterPresent) {
        if (result && result.length) {
          result = result.filter((data) => {
            return checkedFilters.indexOf(data.origin.name) > -1
          })
        } else {
          result = contentData?.results.filter((data) => {
            return checkedFilters.indexOf(data.origin.name) > -1
          })
        }
      }
      setCharacterData(result)
    }
    // else no filters present set state data to data from context api
    else setCharacterData(contentData?.results)
  }
  return (
    <main>
      <div className='filtersection'>
        <FilterSection
          selectedFilters={selectedFilters}
          gender={gender}
          species={species}
          origin={origin}
          staticData={staticData}
          handleFilters={handleFilters}
        />
      </div>
      <div className='searchsection'>
        <div className='searchsection-filters'>
          <p className='searchsection-header-text'>
            {staticData?.search?.filter_text}
          </p>
          <div className='filters-container'>
            {selectedFilters && selectedFilters.length ? (
              selectedFilters.map((filter) => (
                <div className='individual-filter' key={filter}>
                  <p data-testid='applied-filters'>{filter}</p>
                  <Button
                    testId='remove-filter'
                    onClick={() => removeFilter(filter)}
                    buttonStyle='remove-filter-button'
                  >
                    X
                  </Button>
                </div>
              ))
            ) : (
              <Fragment />
            )}
          </div>
        </div>
        <div className='searchsection-sort'>
          <Search setSearchedText={setSearchedText} search={search} />
          <div className='sort'>
            <Sort onChange={sort} />
          </div>
        </div>
      </div>
      <div className='contentsection'>
        {characterData && characterData.length ? (
          <CharacterList characterData={characterData} />
        ) : (
          <div className='empty-box'>No Data Found</div>
        )}
      </div>
    </main>
  )
}
export default Homepage
