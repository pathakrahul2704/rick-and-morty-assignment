import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import React from 'react'
import Home from './Home'
import { mockResponse } from '../../../../__mocks__/mock'
import { Provider } from '../../../store/store'
import staticData from '../../../../public/content/en-us.json'
const contentData = mockResponse

describe('Home.jsx', () => {
  it('render home', () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
  })

  test('sort character', async () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
    const selectBox = screen.getByTestId('character-sort')
    fireEvent.change(selectBox, {
      target: { value: 'asc' }
    })
    expect(screen.getByTestId('character-asc').selected).toBe(true)
    expect(screen.getByTestId('character-dsc').selected).toBe(false)

    fireEvent.change(selectBox, {
      target: { value: 'dsc' }
    })
    expect(screen.getByTestId('character-asc').selected).toBe(false)
    expect(screen.getByTestId('character-dsc').selected).toBe(true)
  })
  test('toggle checkbox for species  ', async () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
    const speciesCheckbox = screen.getAllByTestId('species-filter')
    const genderCheckbox = screen.getAllByTestId('gender-filter')
    const originCheckbox = screen.getAllByTestId('origin-filter')

    expect(speciesCheckbox[0]).not.toBeChecked()
    expect(genderCheckbox[0]).not.toBeChecked()
    expect(originCheckbox[0]).not.toBeChecked()
    expect(speciesCheckbox[1]).not.toBeChecked()

    fireEvent.click(speciesCheckbox[0])
    fireEvent.click(genderCheckbox[0])
    fireEvent.click(originCheckbox[0])
    fireEvent.click(speciesCheckbox[1])

    expect(speciesCheckbox[0]).toBeChecked()
    expect(genderCheckbox[0]).toBeChecked()
    expect(originCheckbox[0]).toBeChecked()
    expect(speciesCheckbox[1]).toBeChecked()

    waitFor(() => {
      expect(screen.queryByText('Earth (C-137)')).toBeInTheDocument()
    })

    let appliedFilters, removeFilterButtons
    appliedFilters = await screen.findAllByTestId('applied-filters')
    removeFilterButtons = await screen.findAllByTestId('remove-filter')

    await expect(appliedFilters[0]).toHaveTextContent('Human')
    await expect(appliedFilters[1]).toHaveTextContent('Male')

    await fireEvent.click(removeFilterButtons[0])

    appliedFilters = await screen.findAllByTestId('applied-filters')
    await expect(appliedFilters[0]).toHaveTextContent('Male')

    fireEvent.click(speciesCheckbox[1])
  })

  test('search character', async () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
    const searchInput = screen.getByTestId('search-input')
    fireEvent.change(searchInput, {
      target: { value: 'rick' }
    })
    fireEvent.click(screen.getByTestId('search-button'))
    waitFor(() => {
      expect(screen.queryByText('Rick')).toBeInTheDocument()
    })

    // condition when input value null and filter selected
    const speciesCheckbox = screen.getAllByTestId('species-filter')
    await fireEvent.click(speciesCheckbox[0])
    await fireEvent.change(searchInput, {
      target: { value: null }
    })
  })

  test('toggle checkbox for origin', async () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
    const originCheckbox = screen.getAllByTestId('origin-filter')

    expect(originCheckbox[0]).not.toBeChecked()

    fireEvent.click(originCheckbox[0])

    expect(originCheckbox[0]).toBeChecked()

    waitFor(() => {
      expect(screen.queryByText('Earth (C-137)')).toBeInTheDocument()
    })

    let appliedFilters
    appliedFilters = await screen.findAllByTestId('applied-filters')

    await expect(appliedFilters[0]).toHaveTextContent('Earth (C-137)')
  })
  test('if filters not selected', async () => {
    render(
      <Provider value={{ staticData, contentData }}>
        <Home />
      </Provider>
    )
    const speciesCheckbox = screen.getAllByTestId('species-filter')

    expect(speciesCheckbox[0]).not.toBeChecked()
    fireEvent.click(speciesCheckbox[0])
    expect(speciesCheckbox[0]).toBeChecked()
    fireEvent.click(speciesCheckbox[0])
    waitFor(() => {
      expect(screen.queryByText('Rick')).toBeInTheDocument()
    })
  })
})
