import { createContext } from 'react'
export const LocalStateContext = createContext()
export const Provider = LocalStateContext.Provider
export const Consumer = LocalStateContext.Consumer
export default LocalStateContext
