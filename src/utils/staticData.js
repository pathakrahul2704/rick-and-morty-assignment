export const species = [
  {
    id: 1,
    name: 'Human'
  },
  {
    id: 2,
    name: 'Alien'
  }
]
export const gender = [
  {
    id: 1,
    name: 'Male'
  },
  {
    id: 2,
    name: 'Female'
  }
]
export const origin = [
  {
    id: 1,
    name: 'Earth (C-137)'
  },
  {
    id: 2,
    name: 'Earth (Replacement Dimension)'
  },
  { id: 3, name: 'Abadango' },
  { id: 4, name: 'unknown' }
]
